<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $guarded = [];

    protected $appends = ['image'];

    public function hotel()
    {
        return $this->belongsTo('App\Hotel');
    }
    public function booking()
    {
        return $this->hasMany('App\Booking');
    }
    public function room_type()
    {
        return $this->belongsTo('App\RoomType');
    }
    public function room_name()
    {
        return $this->belongsTo('App\RoomName');
    }
    public function getImageAttribute()
    {
        $directory = "/uploads/room/".$this->id;
        $images = \File::glob($directory . "/*.jpg");
        if(count($images) > 0)
        {
            return $images;
        }
        return [asset('images/hotel-1.png')];
    }
}
