<?php

namespace App\Http\Controllers;

use App\Partner;
use App\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Booking;
use App\Hotel;
use Auth;
class BookingController extends Controller
{
    public function index($id)
    {
        if(Auth::user()->hasRole('admin')) {
            $hotels = Hotel::all();
        }
        else if(Auth::user()->hasRole('manager')) {
            $hotels = Hotel::where('user_id',Auth::user()->id)->get();
        }
        $bookings = Booking::where('hotel_id', $id)->get();
        return view('backend.booking.index',compact('bookings','id','hotels'));
    }
    public function add($id)
    {
        if(Auth::user()->hasRole('admin')) {
            $hotels = Hotel::all();
        }
        else if(Auth::user()->hasRole('manager')) {
            $hotels = Hotel::where('user_id',Auth::user()->id)->get();
        }
        $data = Booking::where('room_id', $id)->get();
        return view('backend.booking.add',compact('data','id','hotels'));
    }
    public function create($id)
    {
        $room_ids = request('room');
        $rooms_count = request('count');
        $rooms = [];
        foreach ($room_ids as $key => $room_id){
            if($rooms_count[$key] != 0){
                foreach(range(1,$rooms_count[$key]) as $index) {
                    array_push($rooms,Room::find($room_id));
                }
            }
        }
        if(count($rooms) == 0){
            return redirect()->back()->with('success','Не выбрано ничего');
        }
        $checkin_date = request('from');
        $checkout_date = request('to');
        $from_bookings = Booking::where('hotel_id',$id)->whereBetween('from',[$checkin_date,$checkout_date])->get();
        $to_bookings = Booking::where('hotel_id',$id)->whereBetween('to',[$checkin_date,$checkout_date])->get();
        $bookings = $from_bookings->merge($to_bookings);
        $partners = Partner::where('hotel_id',$id)->get();
        return view('backend.booking.create',compact('id','rooms','bookings','rooms_count','partners','checkin_date','checkout_date'));
    }
    public function edit($id)
    {
        $booking = Booking::findOrFail($id);
        $room = $booking->room;
        $id = $room->hotel->id;
        $partners = Partner::where('hotel_id',$id);
        return view('backend.booking.edit',compact('booking','partners','room','id'));
    }
    public function show($id)
    {
        $data = Booking::findOrFail($id);
        return view('backend.booking.show',compact('data'));
    }
    public function get($id){
        if(Auth::user()->hasRole('admin')) {
            $hotels = Hotel::all();
        }
        else if(Auth::user()->hasRole('manager')) {
            $hotels = Hotel::where('user_id',Auth::user()->id)->get();
        }
        $checkin_date = Carbon::parse(request('from'));
        $checkout_date = Carbon::parse(request('to'));
        $from_bookings = Booking::where('hotel_id',$id)->whereBetween('from',[$checkin_date,$checkout_date])->get();
        $to_bookings = Booking::where('hotel_id',$id)->whereBetween('to',[$checkin_date,$checkout_date])->get();
        $booking = $from_bookings->merge($to_bookings);
        $rooms = Room::where('hotel_id',$id)->get();
        $count = 0;
        foreach ($rooms as $room){
            if(($room->room_count - $booking->where('room_id',$room->id)->count()) > 0){
                $count++;
            }
        }
        if($count == 0){
            return redirect()->back()->with('error','Нет свободых номеров по задданой дате');
        }
        return view('backend.booking.get',compact('booking','checkin_date','checkout_date','rooms','id','hotels'))->with('success','Успешно добавлено');
    }
    public function getRooms($id){
        $hotel = Hotel::findOrFail($id);
        $checkin_date = Carbon::parse(request('from'));
        $checkout_date = Carbon::parse(request('to'));
        $from_bookings = Booking::where('hotel_id',$hotel->id)->whereBetween('from',[$checkin_date,$checkout_date])->get();
        $to_bookings = Booking::where('hotel_id',$hotel->id)->whereBetween('to',[$checkin_date,$checkout_date])->get();
        $booking = $from_bookings->merge($to_bookings);
        $rooms = Room::where('hotel_id',$hotel->id)->with('room_type','room_name')->get();
        $available_rooms = $rooms->map(function($room) use($booking) {
            $getBookingCount = $booking->where('room_id',$room->id)->count();
            if((($room->room_count * 1) - $getBookingCount) > 0){
                $room->room_count = ($room->room_count * 1) - $getBookingCount;
                return $room;
            }
        });

        return response()->json($available_rooms);
    }
    public function store(Request $request, $id)
    {
        $room_ids = $request->room_id;
        foreach ($room_ids as $key => $room_id){
            $room = Room::findOrFail($room_id);
            Booking::create([
                'from'=> Carbon::parse($request->from),
                'to'=> Carbon::parse($request->to),
                "room_id" => $room_id,
                "hotel_id" => $room->hotel_id,
                "numeration" => $room->numeration_enabled ? $request->numeration[$key] : 0,
                "name" => $request->name[$key],
                "adult" => $request->adult[$key],
                "child" => $request->child[$key],
                "price" => (json_decode($room->price,true)[(($request->adult[$key] * 1) - 1)] * 1),
                "phone" => $request->phone[$key],
                "email" => $request->email[$key],
                "partner_id" => $request->partner_id[$key],
            ]);
        }
        return redirect()->action('BookingController@index',$id)->with('success','Успешно добавлено');
    }
    public function book(Request $request, $id)
    {
        $room_ids = $request->room_id;
        foreach ($room_ids as $key => $room_id){
            $room = Room::findOrFail($room_id);
            Booking::create([
                'from'=> Carbon::parse($request->from),
                'to'=> Carbon::parse($request->to),
                "room_id" => $room_id,
                "hotel_id" => $room->hotel_id,
                "numeration" => $room->numeration_enabled ? $request->numeration[$key] : 0,
                "name" => $request->name[$key],
                "adult" => $request->adult[$key],
                "child" => $request->child[$key],
                "price" => (json_decode($room->price,true)[(($request->adult[$key] * 1) - 1)] * 1),
                "phone" => $request->phone[$key],
                "email" => $request->email[$key],
                "partner_id" => $request->partner_id[$key],
            ]);
        }
        return redirect()->action('BookingController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        $booking = Booking::find($id);
        $room = Room::findOrFail($booking->room_id);
        $numeration = $room->numeration_enabled;
        $adult = (($request->adult * 1) - 1);
            $booking->update([
                "numeration" => $numeration ? $request->numeration : 0,
                "name" => $request->name,
                "adult" => $request->adult,
                "child" => $request->child,
                "price" => (json_decode($room->price,true)[$adult]),
                "phone" => $request->phone,
                "email" => $request->email,
                "partner_id" => $request->partner_id,
            ]);
        return redirect()->back()->with('success','Успешно добавлено');
    }
    public function delete($id)
    {
        $booking = Booking::findOrFail($id);
        $booking->delete();
        return redirect()->back()->with('success','Успешно удален');
    }
    public function switch(Request $request, $id)
    {
        $hotel = Booking::findOrFail($id);
        $hotel->update([
            'status'=> !$hotel->status
        ]);
        return redirect()->back()->with('success','Изменения успешно внесены');
    }
}
