<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Information;

class InformationController extends Controller
{
    public function index($id)
    {
        $data = Information::where('type_id',$id)->get();
        $info = Type::find($id);
        return view('backend.information.index',compact('data','info','id'));
    }
    public function create($id)
    {
        return view('backend.information.create', compact('id'));
    }
    public function edit($id)
    {
        $data = Information::findOrFail($id);
        return view('backend.information.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = Information::findOrFail($id);
        return view('backend.information.show',compact('data'));
    }
    public function store(Request $request,$id)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        Information::create([
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
            'type_id'=> $id,
        ]);
    return redirect()->action('InformationController@index',$id)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        Information::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('InformationController@index',$id)->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = Information::findOrFail($id);
        $hotel->delete();
        return redirect()->action('InformationController@index',$id)->with('success','Успешно удален');
    }
}
