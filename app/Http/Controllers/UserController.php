<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class UserController extends Controller
{
    public function setting()
    {
        $user = Auth::user();
        return view('backend.users.setting',compact('user'));
    }
    public function update()
    {
        $user = Auth::user();
        $user->update(
            request()->except('current-password','new-password','new-password_confirmation')
        );
        if(request('current-password') && request('new-password')){
        if (!(\Hash::check(request('current-password'), Auth::user()->password))) {
            return redirect()->back()->with("error","Ваш текущий пароль не совпадает с предоставленным вами паролем. Пожалуйста, попробуйте еще раз.");
        }

        if(strcmp(request('current-password'), request('new-password')) == 0){
            return redirect()->back()->with("error","Новый пароль не может совпадать с вашим текущим паролем. Пожалуйста, выберите другой пароль.");
        }

            request()->validate([
                'current-password' => 'required',
                'new-password' => 'required|string|min:6|confirmed',
            ]);
            $user = Auth::user();
            $user->password = bcrypt(request('new-password'));
            $user->save();
        }
        return redirect()->back()->with("success","Данные успешно изменены !");
    }
}
