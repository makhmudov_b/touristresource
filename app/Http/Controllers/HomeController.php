<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Hotel;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(!Auth::user()->hasRole('admin')){
            if(Auth::user()->hotels()->first() == null){
                return redirect()->action('HotelController@create');
            }
            if(Auth::user()->hotels()->first()->rooms()->first() == null){
                return redirect()->action('RoomController@create',Auth::user()->hotels()->first()->id);
            }
        }
    if(Auth::user()->hasRole('admin')) {
        $hotels = Hotel::all();
    }
    else if(Auth::user()->hasRole('manager')) {
        $hotels = Hotel::where('user_id',Auth::user()->id)->get();
    }
    else{
        $hotels = null;
    }
        return view('backend.index',compact('hotels'));
    }
}
