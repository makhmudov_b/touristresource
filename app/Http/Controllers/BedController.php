<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bed;

class BedController extends Controller
{
    public function index()
    {
        $data = Bed::all();
        return view('backend.bed.index',compact('data'));
    }
    public function create()
    {
        return view('backend.bed.create');
    }
    public function edit($id)
    {
        $data = Bed::findOrFail($id);
        return view('backend.bed.edit',compact('data'));
    }
    public function show($id)
    {
        $data = Bed::findOrFail($id);
        return view('backend.bed.show',compact('data'));
    }
    public function store(Request $request)
    {
        request()->validate([
            'name_ru' => 'required',
            'name_en' => 'required',
        ]);
        Bed::create([
            'name_ru'=>$request->name_ru,
            'name_en'=>$request->name_en,
        ]);
    return redirect()->action('BedController@index')->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        Bed::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('BedController@index')->with('success','Изменения успешно внесены');
    }
    public function delete($id)
    {
        $hotel = Bed::findOrFail($id);
        $hotel->delete();
        return redirect()->action('BedController@index')->with('success','Успешно удален');
    }
}
