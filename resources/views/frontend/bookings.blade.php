@extends('layouts.frontend')

@section('content')
@include('front_partials.nav-light')
@include('front_partials.nav-primary')
@include('front_partials.message')
<div class="bookings">
    <div class="container">
        <div class="row">
        @foreach($hotels as $hotel)
        <div class="bookings__item d-flex flex-wrap align-items-center w-100">
            <div class="left"><img src="{{$hotel->image[0]}}" alt="photo"></div>
            <div class="right d-flex flex-column justify-content-between">
                <div class="top d-flex align-items-center justify-content-between">
                    <div class="city">{{ $hotel->city->name_en .' , '. $hotel->city->country->name_en ?? ' '}}</div>
                    <div class="persons">{{$hotel->getBookingInformation()['adults']}} persons</div>
                </div>
                <div class="middle d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-center">
                    <div class="title">{{$hotel->name}}</div>
                    <div class="rooms">{{ $hotel->getBookingInformation()['rooms'] }} rooms</div>
                </div>
                <div class="bottom d-flex align-items-lg-center align-items-end justify-content-between">
                    <div class="links d-lg-block d-flex flex-column">
                        <a href="{{action('PageController@details',$hotel->id)}}">View reservation</a>
                        <a href="#">Confirmation</a>
                        <a href="{{action('PageController@cancelHotel',$hotel->id)}}">Cancel</a>
                    </div>
                    <div class="total d-lg-block d-flex flex-column"><span>Total amount</span> <span>USD {{$hotel->getBookingInformation()['price']}}</span></div>
                </div>
            </div>
        </div>
        @endforeach
        </div>
    </div>
</div>
@endsection

@section('script')
<script></script>
@endsection
