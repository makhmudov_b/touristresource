@extends(Auth::user()->hotels()->first() ? 'layouts.backend': 'layouts.app')

@section('content')
@if(Auth::user()->hotels()->first())
    @include('partials.header')
@endif
<section>
    <div class="container">
        <div class="row">
        <form action="{{ action('PartnerController@update', $data->id) }}" method="POST" enctype="multipart/form-data" id="form">
            @csrf
            @method('PUT')
                <div class="white-block mb-30">
                    <div class="content secondary">
                        <div class="input-block">
                        <div class="input">
                          <p>Наименование</p>
                            <input type="text" name="name" value="{{$data->name}}" class="regStepOne" id="regUserName" required="required" />
                        </div>
                            <div class="text-block">
                                <p>В дальнейшем, вы сможете указывать партнера при добавлении брони.</p>
                            </div>
                        </div>
                        <div class="input-block">
                            <div class="input half">
                                <div class="half-block max-992 mobile-mb-30">
                                    <p>Контактный телефон</p>
                                    <input required="required" type="number" name="phone" value="{{$data->phone}}" class="form-control regStepOne" min="7" id="regUserPhone">
                                </div>
                                <div class="half-block max-992">
                                    <p>E-mail</p>
                                    <input required="required" type="email" name="email" value="{{$data->email}}" class="form-control regStepOne" id="regUserEmail">
                                </div>
                            </div>
                        </div>
                        <div class="input-block">
                        <div class="input">
                          <p>Реквизиты или другая информация</p>
                            <textarea name="description" cols="30" rows="10">{{$data->description}}</textarea>
                        </div>
                        </div>
                    </div>
                </div>
            <div class="button-block">
                <button class="continue-btn" type="submit">Сохранить</button>
            </div>
        </form>
        </div>
    </div>
</section>
@endsection
