@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
        <h3 class="card-title">Города</h3>
         <a class="btn btn-sm btn-outline-success" href="{{ action('CityController@create',$id) }}">Добавить</a>
        </div>
        <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
        <tr>
                <th>№</th>
                <th>Название</th>
                <th>Название (EN)</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datas)
            <tr>
                <td>#{{ $datas->id }}</td>
                <td>{{ $datas->name_ru }}</td>
                <td>{{ $datas->name_en }}</td>
                <td class="text-center">
                    <a href="{{ action('CityController@edit' , $datas->id) }}" class="btn btn-primary">
                        <i class="fe fe-edit"></i>
                    </a>
{{--                    <form class="d-inline-block" action="{{ action('CityController@delete' , $datas->id) }}" onclick="return confirm('Вы уверены?')" method="POST">--}}
{{--                    @method('DELETE')--}}
{{--                    @csrf--}}
{{--                    <button type="submit" class="btn btn-danger">--}}
{{--                        <i class="fe fe-trash"></i>--}}
{{--                    </button>--}}
{{--                    </form>--}}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection
