@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Город</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CityController@store',$id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="name-en">Название (EN)</label>
                            <input name="name_en" required="required" type="text" class="form-control" id="name-en" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="name-ru">Название (RU)</label>
                            <input name="name_ru" required="required" type="text" class="form-control" id="name-ru" placeholder="Введите название">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('backend/js/vendors/chosen.js') }}"></script>
<script>
      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
</script>
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
@endsection
