@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Изменить город</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CityController@update', $data->id) }}" method="POST" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="name">Название (RU)</label>
                            <input value="{{ $data->name_ru }}" required="required" name="name_ru" type="text" class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="desc">Название (UZ)</label>
                            <input value="{{ $data->name_en }}" required="required" name="name_en" type="text" class="form-control" id="desc">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
