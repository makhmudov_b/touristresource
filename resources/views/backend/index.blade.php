@extends('layouts.backend')

@section('content')
@include(Auth::user()->hasRole('admin') ? 'partials.admin_header':'partials.hotel_header')
	<section class="jv_dashboard_section">
		<div class="container">
			<div class="row">
				<h2 class="blue-title">Мои объекты</h2>
				<form action="#" autocomplete="off">
					<div class="white-block hotels">
						<ul class="list-block">
                            @foreach($hotels as $hotel)
							<li class="item">
								<a class="left-block" href="{{ action('BookingController@index',$hotel->id) }}">
									<i class="fe fe-home"></i>
									<p class="pl-2">{{ $hotel->name  }}</p>
								</a>
								<div class="right-block long">
									<span class="icon">
										<label class="custom-switch">
										  <input type="checkbox" name="custom-switch-checkbox" class="custom-switch-input" @if($hotel->status) checked="checked" @endif>
										  <span class="custom-switch-indicator actioner" data-href="{{action('HotelController@switch',$hotel->id)}}"></span>
										  <span class="custom-switch-description"></span>
										</label>
									</span>
									<div class="icons">
										<a href="{{ action('HotelController@edit',$hotel->id)  }}" class="edit icon"><i class="fe fe-edit"></i></a>
										<span class="trash icon"><i class="fe fe-trash-2"></i></span>
									</div>
								</div>
							</li>
                            @endforeach
							<a class="add-list-btn" href="{{ action('HotelController@create') }}">
								<i class="fe fe-plus-circle"></i>
								Добавить обьект
							</a>
						</ul>
					</div>
					<div class="button-block">
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection

@section('script')
<script>
    $('.actioner').on('click',function () {
        let url = $(this).data('href');
        $.get(url);
    })
</script>
@endsection
