@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Категории</h3>
    <a class="btn btn-sm btn-outline-success" href="{{ action('CategoryController@create') }}">Добавить</a>
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th>Название</th>
            <th>Название (EN)</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td>{{ $datas->name_ru }}</td>
            <td>{{ $datas->name_en }}</td>
            <td class="text-right">
                <a href="{{ action('CategoryController@edit' , $datas->id) }}" class="btn btn-primary">
                    <i class="fe fe-edit"></i>
                </a>
            </td>
{{--            <td>--}}
{{--            <form action="{{ action('CategoryController@delete' , $datas->id) }}" method="POST">--}}
{{--            @method('DELETE')--}}
{{--            @csrf--}}
{{--                <button class="btn icon border-0" onclick="return confirm('Вы уверены?')">--}}
{{--                    <i class="fe fe-trash"></i>--}}
{{--                </button>--}}
{{--            </form>--}}
{{--            </td>--}}
        </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
</div>
@endsection
