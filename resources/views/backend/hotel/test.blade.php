@extends('layouts.backend')

@section('content')
@include('partials.hotel_header')
<div class="container">
    <div class="row justify-content-center">
            <div class="card">
                <div class="card-header">Добавить Отель</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('HotelController@update',$data->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                        <div class="form-group">
                            <label class="form-label" for="food-name">Название</label>
                            <input name="name" value="{{ $data->name }}" required="required" type="text" class="form-control" id="food-name" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="food-desc">Описание</label>
                            <input name="description" value="{{ $data->description }}" required="required" type="text" class="form-control" id="food-desc" placeholder="Введите описание">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="address">Адресс</label>
                            <input name="address" value="{{ $data->address }}" required="required" type="text" class="form-control" id="address" placeholder="Введите описание">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="phone">Телефон</label>
                            <input name="phone" value="{{ $data->phone }}" required="required" type="text" class="form-control" id="phone" placeholder="Введите описание">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Выберите город </label>
                            <select name="city_id" class="form-control">
                                @foreach( $city as $datas )
                                <option value="{{ $datas->id }}" @if($data->city_id == $datas->id) selected @endif>{{ $datas->name_ru }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="rating">Рейтинг по кол-ву звезд</label>
                            <select name="rating" class="form-control" id="rating">
                                <option value="0" @if($data->rating == 0) selected @endif>
                                    без оценки
                                </option>
                                <option value="1" @if($data->rating == 1) selected @endif>
                                    1
                                </option>
                                <option value="2" @if($data->rating == 2) selected @endif>
                                    2
                                </option>
                                <option value="3" @if($data->rating == 3) selected @endif>
                                    3
                                </option>
                                <option value="4" @if($data->rating == 4) selected @endif>
                                    4
                                </option>
                                <option value="5" @if($data->rating == 5) selected @endif>
                                    5
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="type">Выберите тип</label>
                            <select name="type_id" class="form-control" id="type">
                                @foreach( $type as $datas )
                                <option value="{{ $datas->id }}" @if($data->type_id == $datas->id) selected @endif>
                                    {{ $datas->text_ru }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Регистрация заезда</label>
                            <select name="income" class="form-control">
                                @foreach ($times as $time)
                                <option value="{{$time->format('H:i')}}" @if($data->income == $time->format('H:i')) selected @endif>{{$time->format('H:i')}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Регистрация отьезда</label>
                            <select name="outcome" class="form-control">
                                @foreach ($times as $time)
                                <option value="{{$time->format('H:i')}}" @if($data->outcome == $time->format('H:i')) selected @endif >{{$time->format('H:i')}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Документы, обязательные при регистрации</label>
                            <label class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" class="custom-control-input" name="confirmation" value="1" @if($data->confirmation == 1) checked="checked" @endif />
                                <span class="custom-control-label">Удостоверение личности с фотографией</span>
                            </label>
                            <label class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" class="custom-control-input" name="credit" value="1" @if($data->credit == 1) checked="checked" @endif />
                                <span class="custom-control-label">Кредитная карта</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Карты, принимаемые к оплате</label>
                            <label class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" class="custom-control-input" name="cards[]" value="mastercard" @if(in_array('mastercard',json_decode($data->cards))) checked="checked" @endif />
                                <span class="custom-control-label">Mastercard</span>
                            </label>
                            <label class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" class="custom-control-input" name="cards[]" value="visa" @if(in_array('visa',json_decode($data->cards))) checked="checked" @endif />
                                <span class="custom-control-label">Visa</span>
                            </label>
                            <label class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" class="custom-control-input" name="cards[]" value="unionpay" @if(in_array('unionpay',json_decode($data->cards))) checked="checked" @endif />
                                <span class="custom-control-label">UnionPay</span>
                            </label>
                            <label class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" class="custom-control-input" name="cards[]" value="jcb" @if(in_array('jcb',json_decode($data->cards))) checked="checked" @endif />
                                <span class="custom-control-label">JCB</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Разрешается ли брать с собой животных?</label>
                                <label class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="animals" value="0"  @if($data->animals == 0) checked="checked" @endif />
                                    <span class="custom-control-label">Не разрешается</span>
                                </label>
                                <label class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" name="animals" value="1" @if($data->animals == 1) checked="checked" @endif />
                                    <span class="custom-control-label">Разрешается</span>
                                </label>
                        </div>
                        <h4>Предоставляемые услуги и удобства</h4>
                        <p>Укажите предоставляемые на территории отеля услуги и удобства. Отметье, если они оплачиваются отдельно.</p>
                        <div class="form-group">
                            <div class="info d-flex justify-content-between align-items-center bg-gray">
                                <h5>Тип услуг</h5>
                                <h5>Оплачивается отдельно</h5>
                            </div>
                            <label class="form-label">Опции</label>
                            <div class="custom-switches-stacked">
                                @foreach($service as $item)
                                <label>{{ $item->name_ru }}</label>
                                    @foreach($item->inners as $inner)
                                    <div class="d-flex justify-content-between shadow-sm p-2 checkboxes">
                                        <label class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input action" name="service[]" @if(!empty(json_decode($data->services,true)[$inner->id])) @if(json_decode($data->services,true)[$inner->id] > 0) checked  value="[{{json_decode($data->services,true)[$inner->id]}},{{$inner->id}}]" @endif @endif  data-info="{{ $inner->id }}" />
                                            <span class="custom-control-label">{{ $inner->name_ru }}</span>
                                        </label>
                                        <label class="custom-switch">
                                            <input type="checkbox" value="1" @if(!empty(json_decode($data->services,true)[$inner->id])) @if(json_decode($data->services,true)[$inner->id] > 1) checked @endif @endif class="custom-switch-input changer">
                                            <span class="custom-switch-indicator"></span>
                                        </label>
                                    </div>
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Локация</label>
                            <div id="map" style="height: 400px; width: 500px;"></div>
                            <input required="required" id="long" value="69.240562" name="long" type="hidden" class="form-control">
                            <input required="required" id="lat" value="41.311081" name="lat" type="hidden" class="form-control">
                        </div>
                        <div class="form-group">
                            <div class="d-flex justify-content-between">
                                @foreach($images as $image)
                                    <img src="{{asset($image)}}" alt="image" width="165">
                                @endforeach
                            </div>
                            <label class="form-label">Фото</label>
                            <input type="file" name="image" multiple class="form-control" >
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
    </div>
</div>
@endsection
@section('script')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('backend/js/vendors/fileupload.min.js') }}"></script>
<script>
$(document).ready(function(){
let checkboxes = $('.checkboxes');
checkboxes.each(function() {
        let action = $(this).find('.action');
        let changer = ($(this).find('.changer'));
        changer.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
        });
        action.click(function(){
            let left = $(this).parent().parent().find('.action');
            let right = $(this).parent().parent().find('.changer');
            if(left.prop('checked') && right.prop('checked')){
                left.val(`[2,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',true);
            }
            if(left.prop('checked') && !right.prop('checked')){
                left.val(`[1,${left.data('info')}]`);
                left.prop('checked',true);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && !right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
            if(!left.prop('checked') && right.prop('checked')){
                left.val(`[0,${left.data('info')}]`);
                left.prop('checked',false);
                right.prop('checked',false);
            }
        });
});
});

var lattitude = $('#lat');
var longitude = $('#long');
function initMap() {
var latlng = new google.maps.LatLng({{$data->lat}}, {{$data->long}});
var map = new google.maps.Map(document.getElementById('map'), {
    center: latlng,
    zoom: 12,
        animation:google.maps.Animation.BOUNCE
});
var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    draggable: true
});
var lat ,long;
google.maps.event.addListener(marker, 'dragend', function (event) {
    lat  = this.getPosition().lat().toFixed(6);
    long = this.getPosition().lng().toFixed(6);
    lattitude.val(lat);
    longitude.val(long);
});
}
</script>
@endsection
