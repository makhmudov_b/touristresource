@extends('layouts.backend')

@section('content')
@include('partials.header')
	<section>
		<div class="container">
			<div class="row">
				<form action="">
					<div class="white-block bronItem mb-30">
						<div class="head">
                            <h3>
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus-circle"><circle cx="12" cy="12" r="10"></circle><line x1="12" y1="8" x2="12" y2="16"></line><line x1="8" y1="12" x2="16" y2="12"></line></svg>Новая бронь</h3>
							<input type="text" name="daterange" class="inputDatePicker" id="datepick" value="" />
						</div>
					</div>
				</form>
				<form action="">
					<div class="white-block bronItem mb-30">
						<div class="head jv_head">
                            <h3>
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>Текущие брони</h3>
						</div>
						<div class="jv_content_head">
							<div class="jv_form_group">
								<select name="timeFilter" class="form-control custom-select jv_select">
									<option value="за все время">за все время</option>
									<option value="Ноябрь, 2019">Ноябрь, 2019</option>
									<option value="Октябрь, 2019">Октябрь, 2019</option>
									<option value="Сентябрь, 2019">Сентябрь, 2019</option>
									<option value="Август, 2019">Август, 2019</option>
									<option value="Июль, 2019">Июль, 2019</option>
									<option value="Июнь, 2019">Июнь, 2019</option>
									<option value="Май, 2019">Май, 2019</option>
									<option value="Апрель, 2019">Апрель, 2019</option>
									<option value="Март, 2019">Март, 2019</option>
									<option value="Февраль, 2019">Февраль, 2019</option>
									<option value="Январь, 2019">Январь, 2019</option>
								</select>
							</div>
							<div class="jv_form_group">
								<select name="statusFilter" class="form-control custom-select jv_select">
									<option value="любой статус">любой статус</option>
									<option value="Заселен">Заселен</option>
									<option value="Ожидание">Ожидание</option>
									<option value="Отмена/не заезд">Отмена/не заезд</option>
								</select>
							</div>
							<div class="jv_form_group">
								<select name="partnerFilter" class="form-control custom-select jv_select">
									<option value="Все партнеры">Все партнеры</option>
									<option value="Tourist Resources">Tourist Resources</option>
									<option value="Travel.uz">Travel.uz</option>
									<option value="Ground Tour">Ground Tour</option>
									<option value="Tourist">Tourist</option>
									<option value="Ostrovok">Ostrovok</option>
									<option value="Свободный туризм">Свободный туризм</option>
									<option value="ЧП Турист Уз">ЧП Турист Уз</option>
									<option value="Uzbekistan Tours">Uzbekistan Tours</option>
									<option value="24Travel">24Travel</option>
									<option value="Путевка.уз">Путевка.уз</option>
								</select>
							</div>
							<div class="jv_form_group">
								<div class="input-icon">
									<input type="text" class="form-control" placeholder="поиск">
									<span class="input-icon-addon">
										<i class="fe fe-search"></i>
									</span>
								</div>
							</div>
						</div>
						<div class="table-responsive">
							<table class="datatable">
								<thead>
									<tr>
										<th>
                                            <div class="jv_table_head_th">Даты
                                                <div class="arrows"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg> </div>
                                            </div>
                                        </th>
										<th>
                                            <div class="jv_table_head_th"> Гости
                                                <div class="arrows"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg> </div>
                                            </div>
                                        </th>
										<th>
                                            <div class="jv_table_head_th"> Номер
                                                <div class="arrows"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg> </div>
                                            </div>
                                        </th>
										<th>
                                            <div class="jv_table_head_th"> Заселен
                                                <div class="arrows"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-up"><polyline points="18 15 12 9 6 15"></polyline></svg> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg> </div>
                                            </div>
                                        </th >
									</tr>
								</thead>
								<tbody>
                                    @foreach($bookings as $booking)
                                        <tr class="{{ !$booking->status ?'status_expectation':'status_populated'}}">
                                            <td>{{ \Carbon\Carbon::parse($booking->from)->format('d/m/y').' - '.\Carbon\Carbon::parse($booking->to)->format('d/m') }}</td>
                                            <td>{{ $booking->name }}</td>
                                            <td>
                                                @if($booking->room()->first()->numeration_enabled)<span class="jv_number_span">{{ $booking->numeration }}</span>@endif
                                                {{$booking->room()->first()->room_type()->first()->name_ru }} {{ $booking->room()->first()->room_name()->first()->name_ru}}</td>
                                            <td data-order="expectation">
                                                <div class="jv_table_icon_btns">
                                                    <label class="custom-switch">
                                                        <input type="checkbox" @if($booking->status) checked="checked" @endif class="custom-switch-input" />
                                                        <span class="custom-switch-indicator actioner" data-href="{{action('BookingController@switch',$booking->id)}}"></span>
                                                    </label>
                                                    <div>
                                                        <a href="{{action('BookingController@edit',$booking->id)}}"><svg xmlns="http://www.w3.org/2000/svg"
                                                                 width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>
                                                            <a href="{{ action('BookingController@delete' , $booking->id) }}" style="cursor: pointer;" onclick="return confirm('Вы уверены?')">
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                     width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-slash"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg>
                                                            </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
								</tbody>
							</table>
							<ul class="jv_status_info">
								<li>Заселен</li>
								<li>Ожидание</li>
								<li>Отмена/не заезд</li>
							</ul>
						</div>
					</div>
			</form>
			</div>
		</div>
	</section>
@endsection

@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="{{asset('backend/plugins/datatables/datatables.css')}}">
@endsection
@section('script')
<script>
    let __origDefine = define;
    define = null;
</script>
<script src="{{ asset('backend/plugins/fullcalendar/js/moment.min.js') }}"></script>
<script src="{{ asset('backend/plugins/datatables/DataTables-1.10.16/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('backend/plugins/filer/jquery.filer.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
$(()=>{
    $('.actioner').on('click',function () {
        let url = $(this).data('href');
        $.get(url);
        let tr = $(this).parent().parent().parent().parent();
        if(tr.hasClass('status_expectation')){
            tr.removeClass('status_expectation');
            tr.addClass('status_populated');
        }
        else if(tr.hasClass('status_populated')){
            tr.removeClass('status_populated');
            tr.addClass('status_expectation');
        }
    })
        $('.jv_select').selectize({});
        $('.datatable').DataTable({
            "oLanguage": {
                "oPaginate": {
                    "sPrevious": "предыдущие",
                    "sNext": "далее",
                }
            }
        });
          $('input[name="daterange"]').on('show.daterangepicker', function(ev, picker) {
            $(this).parents('.bronItem').addClass('open');
            if($('.table-condensed tbody .today').hasClass('start-date') && $('.table-condensed tbody .today').hasClass('end-date')){
              $('.table-condensed tbody .today').removeClass('start-date end-date');
              $('.drp-buttons .applyBtn').attr('disabled', true);
            }
          });
          $('input[name="daterange"]').on('hide.daterangepicker', function(ev, picker) {
            $(this).parents('.bronItem').removeClass('open');
          });
          if ($(window).width() < 991) {
            $('input[name="daterange"]').daterangepicker({
              opens: 'left',
              linkedCalendars: false,
              autoUpdateInput: false,
              applyButtonClasses: 'add-list-btn',
              locale: { applyLabel: '<i class="fe fe-plus-circle"></i>Добавить' },
            }, function(start, end, label) {
               var params = {
                    from: moment(start).format('D-M-Y'),
                    to: moment(end).format('D-M-Y')
                };
                let data = [];
                var esc = encodeURIComponent;
                var query = Object.keys(params)
                .map(k => esc(k) + '=' + esc(params[k]))
                .join('&');
                var url = "{{ action('BookingController@get',$id) }}?" + query;
                window.location.href = url;
            });
            }
        else{
          let today = new Date();
          let dd = String(today.getDate()).padStart(2, '0');
          let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
          let yyyy = today.getFullYear();

          today = mm + '/' + dd + '/' + yyyy;
          $('input[name="daterange"]').daterangepicker({
            opens: 'left',
            autoUpdateInput: false,
            constrainInput:true,
            singleDatePicker: false,
            minDate: today,
            maxSpan: {
              "days": 30
            },
            applyButtonClasses: 'add-list-btn',
            locale: { applyLabel: '<i class="fe fe-plus-circle"></i>Добавить' },
            startDate: moment().startOf('day'),
          }, function(start, end, label) {
               let params = {
                    from: moment(start).format('D-M-Y'),
                    to: moment(end).format('D-M-Y')
                };
                let data = [];
                let esc = encodeURIComponent;
                let query = Object.keys(params)
                .map(k => esc(k) + '=' + esc(params[k]))
                .join('&');
                let url = "{{ action('BookingController@get',$id) }}?" + query;
                window.location.href = url;
          });
         }
})();
    </script>
@endsection
