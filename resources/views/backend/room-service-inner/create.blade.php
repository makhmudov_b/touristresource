@extends('layouts.backend')

@section('content')
@include('partials.admin_header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Сервис</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('RoomServiceInnerController@store',$id) }}" method="POST">
                    @csrf
                        <div class="form-group">
                            <label for="name">Название(RU)</label>
                            <input required="required" name="name_ru" type="text" class="form-control" id="name" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="name-en">Название(EN)</label>
                            <input required="required" name="name_en" type="text" class="form-control" id="name-en" placeholder="Введите название">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
