$(function() {
  $('.jv_menu_list a').on('click', function(e){
    if($(this).hasClass('active')){
      e.preventDefault();

    }
    $(this).parents('.jv_menu_list').toggleClass('open')
    $('.jv_menu_list .active').removeClass('active')
    $(this).addClass('active')

  })
  $("#forgot").css("display","none");
  $("#success").css("display","none");
  $("#returnForgot").click(function(){
       $("#enter").css("display","none");
       $("#forgot").css("display","block");
   });
   $("#returnSuccess").click(function(){
      $("#forgot").css("display","none");
      $("#success").css("display","block");
  });

  // regStepOneValidator
  let allInputs = $('.regStepOne');
  let btnRegStepOne = $('#btnRegStepOne');
  allInputs.on('input', function(){
    let thisElement = $(this);
    let thisElementVal = thisElement.val();
    let thisElementId = thisElement.attr('id');
    thisElement.removeClass('notTouched')
    // checkisNotEmpty
    if(thisElementVal == ''){
      thisElement.addClass('isEmptyInput');
    }else{
      thisElement.removeClass('isEmptyInput');
    }
    if(thisElementId == 'regUserName'){
      if(thisElementVal.length < 2){
        thisElement.addClass('isNotValidInput')
      }else{
        thisElement.removeClass('isNotValidInput')
      }
    }else if(thisElementId == 'regUserEmail'){
     if(IsEmail(thisElementVal)){
        thisElement.removeClass('isNotValidInput')
     }else{
      thisElement.addClass('isNotValidInput')
     }
    }else if(thisElementId == 'regUserPhone'){
      if(IsPhoneNumber(thisElementVal)){
        thisElement.removeClass('isNotValidInput')
     }else{
        thisElement.addClass('isNotValidInput')
     }
    }else if(thisElementId == 'regUserPass'){
      if(thisElementVal.length < 6){
        thisElement.addClass('isNotValidInput')
      }else{
        thisElement.removeClass('isNotValidInput')
      }
    }else if(thisElementId == 'regUserConfirmPass'){
      if(thisElementVal === $('#regUserPass').val()){
        thisElement.removeClass('isNotValidInput')
      }else{
        thisElement.addClass('isNotValidInput')
      }
    }
    // btnDisabledOFF
    if($('.notTouched').length == 0 && $('.isEmptyInput').length == 0 &&  $('.isNotValidInput').length == 0){
      btnRegStepOne.removeAttr('disabled')
    }else{
      btnRegStepOne.attr('disabled', true)
      $('#regStepTwoTabLink').addClass('disabled');
    }

  })
  btnRegStepOne.on('click', function(e){
    e.preventDefault()
    $('#regStepTwoTabLink').removeClass('disabled');
    $('#regStepTwoTabLink').trigger('click');
  })
  // emailValid
  function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
      return false;
    }else{
      return true;
    }
  }
  // IsPhoneNumber
  function IsPhoneNumber(phone) {
    var regex = /([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})/;
      if(!regex.test(phone)) {
        return false;
      }else{
        return true;
      }
  }
    // regStepOneValidator - end
    // regStepTwoValidator
    let btnRegStepTwo = $('#btnRegStepTwo');
    let regStepTwoRadio = $('.regStepTwoRadio');
    let regStepTwoLabel = $('.regStepTwoRadioLabel');
    let regStepTwo = $('.regStepTwo');

    btnRegStepTwo.on('click', function(){
      let regStepTwoRadioChecked = $('.regStepTwoRadio:checked');
      if(regStepTwoRadioChecked.length){
        regStepTwo.each(function(index, elem){
          let thisElem = $(elem);
          let thisElemVal = thisElem.val()
          if(thisElemVal != ''){
            thisElem.removeClass('isEmptyInput');
          }else{
            thisElem.addClass('isEmptyInput');
            $([document.documentElement, document.body]).animate({
              scrollTop: thisElem.offset().top - 300
            }, 1000);
          }
        })
      }else{
        regStepTwoLabel.addClass('isEmptyInput')
        $([document.documentElement, document.body]).animate({
          scrollTop: 100
        }, 1000);
      }
      if(!$('.isEmptyInput').length){
        $('#regStepThreeTabLink').removeClass('disabled');
        $('#regStepThreeTabLink').trigger('click');
      }
    })
    regStepTwoRadio.on('change', function(){
      regStepTwoLabel.removeClass('isEmptyInput')
    })
    regStepTwo.on('input', function(){
      let thisEl = $(this)
      let thisElId = thisEl.attr('id');
      let thisElVal = thisEl.val();

      if(thisElId == 'addressHotel'){
        if(thisElVal.length){
          thisEl.removeClass('isEmptyInput')
        }else{
          thisEl.addClass('isEmptyInput')
        }
      }else if(thisElId == 'phoneHotel'){
        if(IsPhoneNumber(thisElVal)){
          thisEl.removeClass('isEmptyInput')
        }else{
          thisEl.addClass('isEmptyInput')
        }
      }

    })
    // regStepTwoValidator - end
    // file uploader
    $(".filer_input2").filer({
      limit: null,
      maxSize: null,
      extensions: null,
      changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
      showThumbs: true,
      theme: "dragdropbox",
      templates: {
        box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
        item: `<li class="gallryUploadBlock_item jFiler-item">
        {{fi-image}}
        <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
        </li>`,
        progressBar: '<div class="bar"></div>',
        itemAppendToEnd: true,
        canvasImage: true,
        removeConfirmation: false,
        _selectors: {
          list: '.jFiler-items-list',
          item: '.jFiler-item',
          progressBar: '.bar',
          remove: '.fe.fe-minus'
        }
      },
      dragDrop: {
        dragEnter: null,
        dragLeave: null,
        drop: null,
        dragContainer: null,
      },
      files: null,
      addMore: true,
      allowDuplicates: true,
      clipBoardPaste: true,
      excludeName: null,
      beforeRender: null,
      afterRender: null,
      beforeShow: null,
      beforeSelect: null,
      itemAppendToEnd: true,
      onSelect: null,
      afterShow: function(jqEl, htmlEl, parentEl, itemEl){
        $('.galleryAddElement').hide()
        $('.cloneGalleryAddElement').remove()
        $('.jFiler-items-list').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
      },
      onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
        var filerKit = inputEl.prop("jFiler"),
              file_name = filerKit.files_list[id].name;
          // $.post('./php/ajax_remove_file.php', {file: file_name});
          if(filerKit.files_list.length == 1){
            $('.galleryAddElement').show()
          }
      },
      onEmpty: null,
      options: null,
    });
    $('.fotoUploader').on('click', '.cloneGalleryAddElement', function(e){
      $('.galleryAddElement').trigger('click');
    })
        // file uploader-end
        // regStepThreeValidator
        let personNumberInput = $('#personNumber');
        let personNumberPriceBlock = $('#personNumberPriceBlock');
        let internalNumber = $('#internalNumber');
        let internalNumberBlock = $('#internalNumberBlock');
        const pasteElementInput = (index, blockName) =>{

            if(blockName =='personNumber'){
              return (`
                <div class="input half w-auto col-lg-6 pl-0 pb-md-5">
                    <div class="half-block input-group max-992 mobile-mb-30 w-auto">
                      <p>Стоимость размещения ${index} чел.</p>
                        <span class="input-group-prepend" id="basic-addon1">
                          <span class="input-group-text">USD</span>
                        </span>
                        <input type="text" name="price[]" required="required" class="form-control w-auto" data-mask="000.000.000.000.000,00" data-mask-reverse="true" autocomplete="off" maxlength="22">
                    </div>
                </div>
`
              )
            }else{
              return (
                  `<div class="input-block">
                        <div class="input half">
                              <div class="half-block">
                                  <input type="number" name="numeration[]"  value="${index}" disabled >
                              </div>
                              <div class="half-block">
                                  <input type="number" required="required" name="numeration[]"  value="${index}" >
                              </div>
                          </div>
                      </div>`
              )
            }
        }
        personNumberPriceBlock.hide()
        internalNumberBlock.hide()

        personNumberInput.on('input', function(){
          let thisEl = $(this)
          let thisElVal = thisEl.val();
          $('#personNumberPriceBlock .inner').empty();
          if(thisElVal != '' && thisElVal >= 1 && thisElVal <= 20){
            for (let index = 0; index < thisElVal; index++) {
              $('#personNumberPriceBlock .inner').append(pasteElementInput(index+1, 'personNumber'))
            }
            personNumberPriceBlock.show()
          }else{
            personNumberPriceBlock.hide()
          }
        })

        internalNumber.on('input', function(){
          let thisEl = $(this)
          let thisElVal = thisEl.val();
          $('#internalNumberBlock .numeration').empty();

          if(thisElVal != '' && thisElVal >= 1 && thisElVal <= 100){
            for (let index = 0; index < thisElVal; index++) {
              $('#internalNumberBlock .numeration').append(pasteElementInput(index+1, 'internalNumber'))
            }
            internalNumberBlock.show()
          }else{
            internalNumberBlock.hide()
          }
        })
        // regStepThreeValidator-end
});
require(['jquery', 'selectize', 'input-mask'], function ($, selectize) {
  $(document).ready(function () {
    $('.j_select').selectize({});
            $('#input-tags').selectize({
                delimiter: ',',
                persist: false,
                create: function (input) {
                    return {
                        value: input,
                        text: input
                    }
                }
            });

            $('#select-beast').selectize({});
            $('#select-beast1').selectize({});
            $('#select-beast2').selectize({});
            $('#select-beast3').selectize({});
            $('#select-beast4').selectize({});
            $('#select-beast5').selectize({});
            $('#select-beast6').selectize({});
            $('.selecting').selectize({});

            $('#select-users').selectize({
                render: {
                    option: function (data, escape) {
                        return '<div>' +
                            '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                            '<span class="title">' + escape(data.text) + '</span>' +
                            '</div>';
                    },
                    item: function (data, escape) {
                        return '<div>' +
                            '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                            escape(data.text) +
                            '</div>';
                    }
                }
            });

            $('#select-countries').selectize({
                render: {
                    option: function (data, escape) {
                        return '<div>' +
                            '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                            '<span class="title">' + escape(data.text) + '</span>' +
                            '</div>';
                    },
                    item: function (data, escape) {
                        return '<div>' +
                            '<span class="image"><img src="' + data.image + '" alt=""></span>' +
                            escape(data.text) +
                            '</div>';
                    }
                }
            });
  })
})
