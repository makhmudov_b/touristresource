$( ".input-block .text-block" ).each(function() {
  if($(this).children().length == 0) {
    $(this).remove();
  }
});

$('.user').on('click', function() {
    $(this).toggleClass('open');
});
$('.dropdown .name').on('click', function() {
$(this).parent('.dropdown').toggleClass('open');
});

$(document).on('click', function(event) {
  if (!$(event.target).closest(".user").length) {
     $(".user").removeClass('open');
  }
});

$(document).on('click', function(event) {
  if (!$(event.target).closest(".dropdown .name").length) {
     $(".dropdown").removeClass('open');
  }
});

if (window.matchMedia('(max-width: 767px)').matches) {
    var lastItems = $(".object-block .item");
    lastItems.slice(lastItems.length - 2).css("margin-bottom", "0");
}
if (window.matchMedia('(max-width: 575px)').matches) {
    var height = $('.object-block .item').width();

    $('.object-block .item').css('height', height);
}
